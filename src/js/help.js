
   var map = new AMap.Map("container", {
        resizeEnable: true,
        center: [113.562211,22.242966],
        zoom: 13,//地图显示的缩放级别
        keyboardEnable: false
    });

    map.setLang("ch");

    function search(){
     var place=document.getElementsByName("keyword");
     var place_value=place[0].value;
     
    AMap.service(["AMap.PlaceSearch"], function() {
        var placeSearch = new AMap.PlaceSearch({ //构造地点查询类
            pageSize: 12,
            city: "0756", //城市
            map: map,
            panel: "panel"
        });
        //关键字查询
        placeSearch.search(place_value);
    });

    }


       var markers = [
       {position: [113.385933,22.017763]}, 
       {position: [113.552358,22.222465]}, 
       {position: [113.552093,22.217552]},
       {position: [113.550964,22.215868]},
       {position: [113.592001,22.239419]},
       {position: [113.555585,22.236928]},
       {position: [113.510891,22.224599]},
       {position: [113.598183,22.357192]},
       {position: [113.560378,22.278037]},
       {position: [113.535249,22.268302]},
       {position: [113.515488,22.268738]},
       ];

      
       var stationcontent=['航展馆','中珠大厦','来魅力酒店','城轨珠海站', '九洲港客运站','锦江之星（拱北店）','华发商都','聚龙酒店','天鹅大酒店','千鹏酒店','城轨明珠站'];
       var i=0;
         markers.forEach(function(marker) {
        new AMap.Marker({
            map: map,
            position: [marker.position[0], marker.position[1]], 
            label:{ content:stationcontent[i++], offset: new AMap.Pixel(17, 17)}         
        });
         
           
        
    }); 




  