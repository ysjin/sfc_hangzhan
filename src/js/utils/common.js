define(['vue', 'bootstrap'], function (Vue, bootstrap) {

  Vue.component('m-nav', {
    props: ['value'],
    data() {
      return {
        title: ''
      }
    },
    template: `<div class="m-nav">
                  <ul>
                    <li :class="{active: title === 'station_config'}"><a href="./station_config.html">站点配置</a></li>
                    <li :class="{active: title === 'serial_plan'}"><a href="./serial_plan.html">班次计划</a></li>
                    <li :class="{active: title === 'ticket_purchase'}"><a href="./ticket_purchase.html">订票页面</a></li>
                    <li :class="{active: title === 'booking_control'}"><a href="./booking_control.html">订票订座监控</a></li>
                    <li :class="{active: title === 'riding_control'}"><a href="./riding_control.html">乘车监控</a></li>
                    <li :class="{active: title === 'route_control'}"><a href="./route_control.html">车辆运行路线监控</a></li>
                    <li :class="{active: title === 'bus_position_control'}"><a href="./bus_position_control.html">路况和车辆实时位置监控</a></li>
                    <li :class="{active: title === 'park_control'}"><a href="./park_control.html">周边停车场监控</a></li>
                    <li :class="{active: title === 'questionnaire'}"><a href="./questionnaire.html">问卷调查</a></li>
                    <li :class="{active: title === 'company_performance'}"><a href="./company_performance.html">运输公司绩效</a></li>
                    <li :class="{active: title === 'host_performance'}"><a href="./host_performance.html">航展组织方绩效</a></li>
                  </ul>

                </div>`,
    created() {
      this.title = this.value
    }
  })

  return {
    fetch: function (path) {
      return $.ajax({
        type: 'GET',
        url: path,
        data: '',
        headers: {
          'Content-Type': 'application/json'
        },
        success: function (res) {
          if (typeof res === String) {
            res = JSON.parse(res)
          }
          return res
        },
        error: function (err) {
          return err
        }
      })
    },
    // 获取日期
    getDays: function () {
      var newDate = new Date();
      var year = newDate.getFullYear();
      var month = newDate.getMonth() + 1;
      var _hour = newDate.getHours()
      var _mimutes = newDate.getMinutes()
      var _seconds = newDate.getSeconds()
      var firstDay = '01';

      var currentDay = newDate.getDate();
      if (month < 10) {
        month = "0" + month;
      }
      if (currentDay < 10) {
        currentDay = "0" + currentDay;
      }
      var initFromDate = year + "-" + month + "-" + firstDay;
      var initToDate = year + "-" + month + "-" + currentDay;
      // 返回时间
      if (_hour < 10) {
        _hour = "0" + _hour;
      }
      if (_mimutes < 10) {
        _mimutes = "0" + _mimutes;
      }
      if (_seconds < 10) {
        _seconds = "0" + _seconds;
      }
      var _time = _hour + ':' + _mimutes + ':' + _seconds

      return {
        first: initFromDate,   //返回该月的1号日期
        current: initToDate,  // 返回今天
        year: year, // 返回今年
        month: month, // 返回今月
        day: currentDay, // 返回今天日期
        time: _time, // 返回当前时间
        datetime: initToDate + ' ' + _time
      }
    },
    stationPoint: function () {
      return [
        { id: 1, name: '航展馆', coordinate: 'POINT(113.394131,22.024953)' },
        { id: 2, name: '中珠大厦', coordinate: 'POINT(113.558778,22.228316)' },
        { id: 3, name: '来魅力酒店', coordinate: 'POINT(113.558672,22.223624)' },
        { id: 4, name: '城轨珠海站', coordinate: 'POINT(113.555901,22.221111)' },
        { id: 5, name: '九洲港客运站', coordinate: 'POINT(113.599129,22.245427)' },
        { id: 6, name: '锦江之星（拱北店）', coordinate: 'POINT(113.562211,22.242966)' },
        { id: 7, name: '华发商都', coordinate: 'POINT(113.517952,22.231209)' },
        { id: 8, name: '聚龙酒店', coordinate: 'POINT(113.604807,22.363176)' },
        { id: 9, name: '天鹅大酒店', coordinate: 'POINT(113.56693,22.28365)' },
        { id: 10, name: '千鹏酒店', coordinate: 'POINT(113.541042,22.274132)' },
        { id: 11, name: '城轨明珠站', coordinate: 'POINT(113.521717,22.274885)' }
      ]
    },
    busInfo: function () {
      return [
        { id: 1, name: '粤C24402', label: 'bus_c24402', driver: '韦师傅', phone: '13556566565', buspartner: '珠海骏通集团', count: 35, station: '中珠大厦' },
        { id: 2, name: '粤C24401', label: 'bus_c24401', driver: '刘师傅', phone: '13225516552', buspartner: '珠海骏通集团', count: 30, station: '来魅力酒店' },
        { id: 3, name: '粤C24403', label: 'bus_c24403', driver: '张师傅', phone: '13725545412', buspartner: '珠海骏通集团', count: 35, station: '城轨珠海站' },
        { id: 4, name: '粤C19798', label: 'bus_c19798', driver: '关师傅', phone: '13912451135', buspartner: '珠海骏通集团', count: 40, station: '九洲港客运站' },
        { id: 5, name: '粤C19799', label: 'bus_c19799', driver: '杨师傅', phone: '13060475789', buspartner: '珠海骏通集团', count: 42, station: '锦江之星（拱北店）' },
        { id: 6, name: '粤C19801', label: 'bus_c19801', driver: '张师傅', phone: '13876320931', buspartner: '珠海骏通集团', count: 41, station: '华发商都' },
        { id: 7, name: '粤C19802', label: 'bus_c19802', driver: '郑师傅', phone: '18523872365', buspartner: '珠海骏通集团', count: 38, station: '聚龙酒店' },
        { id: 8, name: '粤C19803', label: 'bus_c19803', driver: '谢师傅', phone: '18290492159', buspartner: '珠海骏通集团', count: 37, station: '天鹅大酒店' },
        { id: 9, name: '粤C21543', label: 'bus_c21543', driver: '宋师傅', phone: '18616806773', buspartner: '珠海骏通集团', count: 36, station: '千鹏酒店' },
        { id: 10, name: '粤C21544', label: 'bus_c21544', driver: '曾师傅', phone: '13751874402', buspartner: '珠海骏通集团', count: 40, station: '城轨明珠站' },
        { id: 11, name: '粤C21545', label: 'bus_c21545', driver: '刘师傅', phone: '15112263201', buspartner: '珠海骏通集团', count: 42, station: '斗门' },
        { id: 13, name: '粤C21546', label: 'bus_c21546', driver: '黄师傅', phone: '18689988588', buspartner: '珠海骏通集团', count: 43, station: '红旗曼哈顿酒店' },
        { id: 14, name: '粤C22782', label: 'bus_c22782', driver: '林师傅', phone: '13702640062', buspartner: '珠海骏通集团', count: 42, station: '航展馆' },
        { id: 15, name: '粤C22783', label: 'bus_c22783', driver: '何师傅', phone: '13225516552', buspartner: '珠海骏通集团', count: 46, station: '中珠大厦' },
        { id: 16, name: '粤C22784', label: 'bus_c22784', driver: '许师傅', phone: '13688600647', buspartner: '珠海骏通集团', count: 40, station: '中珠大厦' },
        { id: 17, name: '粤C22785', label: 'bus_c22785', driver: '胡师傅', phone: '13923364282', buspartner: '珠海骏通集团', count: 38, station: '中珠大厦' },
        { id: 18, name: '粤123456', label: 'bus_123456', driver: '谢师傅', phone: '15517571639', buspartner: '珠海骏通集团', count: 39, station: '中珠大厦' }
      ]
    },
    orderInfo: function () {
      return [
{"bus_partner_phone":"8111333","serial_num":"JCJZG1010","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/29","depart_time":"10:10:00","get_on_time":"10:10:00","plate_num":"粤C21542","get_on_station":"珠海珠海机场","take_off_station":"珠海湖心路口","ticket_num":"1","name":"张连文","id_card":"370724198210056000","phone":"13688600647","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCTJ1020","short_name":"珠海机场—唐家","ticket_date":"2017/7/29","depart_time":"10:20:00","get_on_time":"10:20:00","plate_num":"粤C19741","get_on_station":"珠海珠海机场","take_off_station":"珠海天鹅酒店","ticket_num":"1","name":"黄承熙","id_card":"440402199308249000","phone":"13702640062","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCCL1015","short_name":"珠海机场—横琴长隆","ticket_date":"2017/7/29","depart_time":"10:15:00","get_on_time":"10:15:00","plate_num":"粤C21568","get_on_station":"珠海珠海机场","take_off_station":"珠海企鹅酒店","ticket_num":"2","name":"康雨桐","id_card":"350123199006250000","phone":"18689988588","total_price":"60"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1000","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/29","depart_time":"10:00:00","get_on_time":"10:00:00","plate_num":"粤CS2795","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"劳晨邦","id_card":"450721199711193000","phone":"15112263201","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCJM1030","short_name":"珠海机场—江门汽车总站","ticket_date":"2017/7/29","depart_time":"10:30:00","get_on_time":"10:30:00","plate_num":"粤C33883","get_on_station":"珠海珠海机场","take_off_station":"江门江门汽车总站","ticket_num":"1","name":"卢泽基","id_card":"440711198410235000","phone":"13751874402","total_price":"50"},
{"bus_partner_phone":"8111333","serial_num":"JCTJ1020","short_name":"珠海机场—唐家","ticket_date":"2017/7/29","depart_time":"10:20:00","get_on_time":"10:20:00","plate_num":"粤C19741","get_on_station":"珠海珠海机场","take_off_station":"珠海明珠城轨站","ticket_num":"1","name":"张亚龙","id_card":"341222198902029000","phone":"15888023515","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCJZG0940","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/29","depart_time":"9:40:00","get_on_time":"9:40:00","plate_num":"粤C21549","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"2","name":"吴亦琛","id_card":"420204199205134000","phone":"18616806773","total_price":"50"},
{"bus_partner_phone":"8111333","serial_num":"JCTJ0920","short_name":"珠海机场—唐家","ticket_date":"2017/7/29","depart_time":"9:20:00","get_on_time":"9:20:00","plate_num":"粤C18500","get_on_station":"珠海珠海机场","take_off_station":"珠海明珠城轨站","ticket_num":"1","name":"吴斌","id_card":"44040219890925927X","phone":"13326689236","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCTJ0920","short_name":"珠海机场—唐家","ticket_date":"2017/7/29","depart_time":"9:20:00","get_on_time":"9:20:00","plate_num":"粤C18500","get_on_station":"珠海珠海机场","take_off_station":"珠海天鹅酒店","ticket_num":"1","name":"冯祎","id_card":"422126198209132000","phone":"13810147383","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCJZG0910","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/29","depart_time":"9:10:00","get_on_time":"9:10:00","plate_num":"粤C19798","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"姜平","id_card":"320204196707081000","phone":"13961757173","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCZH0900","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/29","depart_time":"9:00:00","get_on_time":"9:00:00","plate_num":"粤C24500","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"4","name":"冉雨静","id_card":"52222719870430002X","phone":"18290492159","total_price":"100"},
{"bus_partner_phone":"8111333","serial_num":"JCZH0900","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/29","depart_time":"9:00:00","get_on_time":"9:00:00","plate_num":"粤C24500","get_on_station":"珠海珠海机场","take_off_station":"珠海南屏","ticket_num":"1","name":"刘洋","id_card":"41040319870715565X","phone":"18523872365","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCZH0845","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/29","depart_time":"8:45:00","get_on_time":"8:45:00","plate_num":"粤C28666","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"钟燕婷","id_card":"460030199110020000","phone":"13876320931","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCTJ0850","short_name":"珠海机场—唐家","ticket_date":"2017/7/29","depart_time":"8:50:00","get_on_time":"8:50:00","plate_num":"粤C16386","get_on_station":"珠海珠海机场","take_off_station":"珠海明珠城轨站","ticket_num":"1","name":"任立业","id_card":"232301197908150000","phone":"13060475789","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCZH0100","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/29","depart_time":"1:00:00","get_on_time":"1:00:00","plate_num":"粤C24421","get_on_station":"珠海珠海机场","take_off_station":"珠海中珠大厦","ticket_num":"1","name":"叶红庆","id_card":"320219196611261000","phone":"13912451135","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCXZY2050","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/28","depart_time":"20:50:00","get_on_time":"20:50:00","plate_num":"粤C24462","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"杜星宇","id_card":"612701199608131000","phone":"18591995066","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCXZY2000","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/28","depart_time":"20:00:00","get_on_time":"20:00:00","plate_num":"粤C19798","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"邹志刚","id_card":"44010519741215063X","phone":"13802804115","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCTJ1620","short_name":"珠海机场—唐家","ticket_date":"2017/7/28","depart_time":"16:20:00","get_on_time":"16:20:00","plate_num":"粤C19798","get_on_station":"珠海珠海机场","take_off_station":"珠海天鹅酒店","ticket_num":"1","name":"黄柏翰","id_card":"340104196212090000","phone":"13232283799","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCTJ1620","short_name":"珠海机场—唐家","ticket_date":"2017/7/28","depart_time":"16:20:00","get_on_time":"16:20:00","plate_num":"粤C19798","get_on_station":"珠海珠海机场","take_off_station":"珠海明珠城轨站","ticket_num":"1","name":"李欢","id_card":"220281198805291000","phone":"15676000343","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCJZG1540","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/28","depart_time":"15:40:00","get_on_time":"15:40:00","plate_num":"粤C15824","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"2","name":"陈珊","id_card":"422202198609263000","phone":"15871782147","total_price":"38"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1530","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/28","depart_time":"15:30:00","get_on_time":"15:30:00","plate_num":"粤C24500","get_on_station":"珠海珠海机场","take_off_station":"珠海南屏","ticket_num":"1","name":"先婷霜","id_card":"510322199611046000","phone":"13824141272","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1500","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/28","depart_time":"15:00:00","get_on_time":"15:00:00","plate_num":"粤C24405","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"何小华","id_card":"511321197906242000","phone":"18779172179","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1500","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/28","depart_time":"15:00:00","get_on_time":"15:00:00","plate_num":"粤C24405","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"朱兰兰","id_card":"14108219940706002X","phone":"15392598266","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1500","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/28","depart_time":"15:00:00","get_on_time":"15:00:00","plate_num":"粤C24405","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"张巍巍","id_card":"232302199012251000","phone":"18645941225","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCJZG1440","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/28","depart_time":"14:40:00","get_on_time":"14:40:00","plate_num":"粤C21543","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"2","name":"谷成","id_card":"430481199010295000","phone":"18670562566","total_price":"50"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1430","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/28","depart_time":"14:30:00","get_on_time":"14:30:00","plate_num":"粤C24421","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"何宇","id_card":"421127198807161000","phone":"15971410973","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1400","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/28","depart_time":"14:00:00","get_on_time":"14:00:00","plate_num":"粤CS2798","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"吴迪","id_card":"21011419931029001X","phone":"18742433353","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCTJ1350","short_name":"珠海机场—唐家","ticket_date":"2017/7/28","depart_time":"13:50:00","get_on_time":"13:50:00","plate_num":"粤C16386","get_on_station":"珠海珠海机场","take_off_station":"珠海聚龙酒店","ticket_num":"1","name":"陈信山","id_card":"420803198202014000","phone":"13823605710","total_price":"30"},
{"bus_partner_phone":"8111333","serial_num":"JCJZG1340","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/28","depart_time":"13:40:00","get_on_time":"13:40:00","plate_num":"粤C21542","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"德合拉","id_card":"623027198511173000","phone":"13210229933","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCTJ1350","short_name":"珠海机场—唐家","ticket_date":"2017/7/28","depart_time":"13:50:00","get_on_time":"13:50:00","plate_num":"粤C16386","get_on_station":"珠海珠海机场","take_off_station":"珠海明珠城轨站","ticket_num":"1","name":"庞玲","id_card":"230402199807220000","phone":"13168830989","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1300","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/28","depart_time":"13:00:00","get_on_time":"13:00:00","plate_num":"粤C24452","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"2","name":"张婧","id_card":"150122199411245000","phone":"17678003265","total_price":"50"},
{"bus_partner_phone":"8111333","serial_num":"JCCL1315","short_name":"珠海机场—横琴长隆","ticket_date":"2017/7/28","depart_time":"13:15:00","get_on_time":"13:15:00","plate_num":"粤C21568","get_on_station":"珠海珠海机场","take_off_station":"珠海横琴湾酒店","ticket_num":"1","name":"姚虹权","id_card":"420381197901310000","phone":"18772800000","total_price":"30"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1245","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/28","depart_time":"12:45:00","get_on_time":"12:45:00","plate_num":"粤C24401","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"蒋才纲","id_card":"431281197003106000","phone":"13976384529","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCTJ1250","short_name":"珠海机场—唐家","ticket_date":"2017/7/28","depart_time":"12:50:00","get_on_time":"12:50:00","plate_num":"粤C19794","get_on_station":"珠海珠海机场","take_off_station":"珠海明珠城轨站","ticket_num":"1","name":"庄松辉","id_card":"350521198111057000","phone":"13519816198","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCJZG1140","short_name":"珠海机场—九洲港","ticket_date":"2017/7/28","depart_time":"11:40:00","get_on_time":"11:40:00","plate_num":"粤C15824","get_on_station":"珠海珠海机场","take_off_station":"珠海锦江之星","ticket_num":"1","name":"赵蕊","id_card":"140107198912303000","phone":"13426005836","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1100","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/28","depart_time":"11:00:00","get_on_time":"11:00:00","plate_num":"粤CS2798","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"肖敏","id_card":"441323199308040000","phone":"13669588083","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCSX1200","short_name":"珠海机场—三乡汽车总站","ticket_date":"2017/7/28","depart_time":"12:00:00","get_on_time":"12:00:00","plate_num":"粤C21576","get_on_station":"珠海珠海机场","take_off_station":"中山三乡候机楼","ticket_num":"1","name":"苏曦浩","id_card":"500383200607242000","phone":"18824988320","total_price":"13"},
{"bus_partner_phone":"8111333","serial_num":"JCSX1200","short_name":"珠海机场—三乡汽车总站","ticket_date":"2017/7/28","depart_time":"12:00:00","get_on_time":"12:00:00","plate_num":"粤C21576","get_on_station":"珠海珠海机场","take_off_station":"中山三乡候机楼","ticket_num":"1","name":"陈乐秀","id_card":"510229197012143000","phone":"18824988320","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCTJ1150","short_name":"珠海机场—唐家","ticket_date":"2017/7/28","depart_time":"11:50:00","get_on_time":"11:50:00","plate_num":"粤C19744","get_on_station":"珠海珠海机场","take_off_station":"珠海聚龙酒店","ticket_num":"1","name":"朱文生","id_card":"445224199306206000","phone":"18688166859","total_price":"30"},
{"bus_partner_phone":"8111333","serial_num":"JCJM1030","short_name":"珠海机场—江门汽车总站","ticket_date":"2017/7/28","depart_time":"10:30:00","get_on_time":"10:30:00","plate_num":"粤C33883","get_on_station":"珠海珠海机场","take_off_station":"江门江门汽车总站","ticket_num":"1","name":"张炜宜","id_card":"440711199712035000","phone":"13045819196","total_price":"30"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1000","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/28","depart_time":"10:00:00","get_on_time":"10:00:00","plate_num":"粤CS2795","get_on_station":"珠海珠海机场","take_off_station":"珠海中珠大厦","ticket_num":"1","name":"陈润东","id_card":"445121199501305000","phone":"18928011995","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCJZG0940","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/28","depart_time":"9:40:00","get_on_time":"9:40:00","plate_num":"粤C21549","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"林金鑫","id_card":"350782199203055000","phone":"18259991070","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCJZG0940","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/28","depart_time":"9:40:00","get_on_time":"9:40:00","plate_num":"粤C21549","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"皮雳","id_card":"511024197407110000","phone":"13317710747","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCCL0915","short_name":"珠海机场—横琴长隆","ticket_date":"2017/7/28","depart_time":"9:15:00","get_on_time":"9:15:00","plate_num":"粤C19744","get_on_station":"珠海珠海机场","take_off_station":"珠海横琴湾酒店","ticket_num":"1","name":"李猛","id_card":"110108198610036000","phone":"15801569651","total_price":"30"},
{"bus_partner_phone":"8111333","serial_num":"JCZH0900","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/28","depart_time":"9:00:00","get_on_time":"9:00:00","plate_num":"粤C24500","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"黄观宝","id_card":"440982198810175000","phone":"17820125901","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCJZG1810","short_name":"珠海机场—九洲港","ticket_date":"2017/7/27","depart_time":"18:10:00","get_on_time":"18:10:00","plate_num":"粤C21543","get_on_station":"珠海珠海机场","take_off_station":"珠海锦江之星","ticket_num":"2","name":"孙鸣阳","id_card":"370302199412100000","phone":"15624481331","total_price":"50"},
{"bus_partner_phone":"8111333","serial_num":"JCJZG1810","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/27","depart_time":"18:10:00","get_on_time":"18:10:00","plate_num":"粤C21543","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"孟成虎","id_card":"421224199410016000","phone":"13114531506","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCTJ1750","short_name":"珠海机场—唐家","ticket_date":"2017/7/27","depart_time":"17:50:00","get_on_time":"17:50:00","plate_num":"粤C19744","get_on_station":"珠海珠海机场","take_off_station":"珠海天鹅酒店","ticket_num":"2","name":"谢思来","id_card":"432501200205080000","phone":"15812605520","total_price":"50"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1530","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/27","depart_time":"15:30:00","get_on_time":"15:30:00","plate_num":"粤C24500","get_on_station":"珠海珠海机场","take_off_station":"珠海中珠大厦","ticket_num":"1","name":"陈钰莹","id_card":"440902200001260000","phone":"18929712070","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCTJ1520","short_name":"珠海机场—唐家","ticket_date":"2017/7/27","depart_time":"15:20:00","get_on_time":"15:20:00","plate_num":"粤C19799","get_on_station":"珠海珠海机场","take_off_station":"珠海聚龙酒店","ticket_num":"2","name":"柯天立","id_card":"441622199608310000","phone":"13192266167","total_price":"30"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1500","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/27","depart_time":"15:00:00","get_on_time":"15:00:00","plate_num":"粤C24405","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"郑兹w理","id_card":"412725198207201000","phone":"18618278576","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1500","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/27","depart_time":"15:00:00","get_on_time":"15:00:00","plate_num":"粤C24405","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"洪波","id_card":"360121198803287000","phone":"18165606605","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1430","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/27","depart_time":"14:30:00","get_on_time":"14:30:00","plate_num":"粤C24421","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"钱迪","id_card":"330225199604190000","phone":"18268518255","total_price":"13"},
{"bus_partner_phone":"8111333","serial_num":"JCJZG1440","short_name":"珠海机场—九洲港","ticket_date":"2017/7/27","depart_time":"14:40:00","get_on_time":"14:40:00","plate_num":"粤C21543","get_on_station":"珠海珠海机场","take_off_station":"珠海吉大九州城","ticket_num":"1","name":"谭俊","id_card":"511227198308235000","phone":"18621899028","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCJZG1410","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/27","depart_time":"14:10:00","get_on_time":"14:10:00","plate_num":"粤C18474","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"许奇钦","id_card":"445381199807286000","phone":"18819834093","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCJZG1410","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/27","depart_time":"14:10:00","get_on_time":"14:10:00","plate_num":"粤C18474","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"张金洁","id_card":"445381199604016000","phone":"15270970353","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1400","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/27","depart_time":"14:00:00","get_on_time":"14:00:00","plate_num":"粤CS2798","get_on_station":"珠海珠海机场","take_off_station":"珠海南屏","ticket_num":"1","name":"吴东州","id_card":"411024198901201000","phone":"15765541210","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCCL1345","short_name":"珠海机场—横琴长隆","ticket_date":"2017/7/27","depart_time":"13:45:00","get_on_time":"13:45:00","plate_num":"粤C21568","get_on_station":"珠海珠海机场","take_off_station":"珠海企鹅酒店","ticket_num":"2","name":"马亥波","id_card":"330103199903092000","phone":"15157196198","total_price":"60"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1245","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/27","depart_time":"12:45:00","get_on_time":"12:45:00","plate_num":"粤C24401","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"李军","id_card":"360124198911031000","phone":"13870841542","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1230","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/27","depart_time":"12:30:00","get_on_time":"12:30:00","plate_num":"粤C24500","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"2","name":"李辉","id_card":"130123197209220000","phone":"13012168011","total_price":"50"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1145","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/27","depart_time":"11:45:00","get_on_time":"11:45:00","plate_num":"粤C28666","get_on_station":"珠海珠海机场","take_off_station":"珠海中珠大厦","ticket_num":"1","name":"许杰平","id_card":"341225199109126000","phone":"13456821663","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCJZG1110","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/27","depart_time":"11:10:00","get_on_time":"11:10:00","plate_num":"粤C19799","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"2","name":"何佳琪","id_card":"511525200605123000","phone":"13560626836","total_price":"38"},
{"bus_partner_phone":"8111333","serial_num":"JCJZG1110","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/27","depart_time":"11:10:00","get_on_time":"11:10:00","plate_num":"粤C19799","get_on_station":"珠海珠海机场","take_off_station":"珠海南屏","ticket_num":"1","name":"邹国平","id_card":"362501197303102000","phone":"13916650925","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1100","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/27","depart_time":"11:00:00","get_on_time":"11:00:00","plate_num":"粤CS2798","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"3","name":"王兴安","id_card":"420502199309011000","phone":"13697297470","total_price":"75"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1100","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/27","depart_time":"11:00:00","get_on_time":"11:00:00","plate_num":"粤CS2798","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"洪晓城","id_card":"441581199610238000","phone":"18814185764","total_price":"13"},
{"bus_partner_phone":"8111333","serial_num":"JCJZG1040","short_name":"珠海机场—九洲港","ticket_date":"2017/7/27","depart_time":"10:40:00","get_on_time":"10:40:00","plate_num":"粤C18474","get_on_station":"珠海珠海机场","take_off_station":"珠海吉大九州城","ticket_num":"1","name":"党亚红","id_card":"610203197305293000","phone":"13032908912","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1030","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/27","depart_time":"10:30:00","get_on_time":"10:30:00","plate_num":"粤C24495","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"陶闻韬","id_card":"330102199010172000","phone":"15158030128","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCTJ1020","short_name":"珠海机场—唐家","ticket_date":"2017/7/27","depart_time":"10:20:00","get_on_time":"10:20:00","plate_num":"粤C19741","get_on_station":"珠海珠海机场","take_off_station":"珠海聚龙酒店","ticket_num":"1","name":"陈展","id_card":"350111199512112000","phone":"15919171134","total_price":"15"},
{"bus_partner_phone":"8111333","serial_num":"JCDM1015","short_name":"珠海机场—斗门","ticket_date":"2017/7/27","depart_time":"10:15:00","get_on_time":"10:15:00","plate_num":"粤C22775","get_on_station":"珠海珠海机场","take_off_station":"珠海斗门","ticket_num":"2","name":"陈诗烨","id_card":"330682199512083000","phone":"18358598264","total_price":"30"},
{"bus_partner_phone":"8111333","serial_num":"JCCL1015","short_name":"珠海机场—横琴长隆","ticket_date":"2017/7/27","depart_time":"10:15:00","get_on_time":"10:15:00","plate_num":"粤C21568","get_on_station":"珠海珠海机场","take_off_station":"珠海横琴湾酒店","ticket_num":"1","name":"许漠垠","id_card":"650104198901130000","phone":"15958170218","total_price":"30"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1030","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/27","depart_time":"10:30:00","get_on_time":"10:30:00","plate_num":"粤C24495","get_on_station":"珠海珠海机场","take_off_station":"珠海中珠大厦","ticket_num":"1","name":"胡哲","id_card":"42112619951104729X","phone":"13216521218","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCJZG1010","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/27","depart_time":"10:10:00","get_on_time":"10:10:00","plate_num":"粤C21542","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"3","name":"陈铭心","id_card":"440181199210024000","phone":"13143338342","total_price":"75"},
{"bus_partner_phone":"8111333","serial_num":"JCJZG1010","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/27","depart_time":"10:10:00","get_on_time":"10:10:00","plate_num":"粤C21542","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"张婷","id_card":"420103198205044000","phone":"15387169699","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1000","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/27","depart_time":"10:00:00","get_on_time":"10:00:00","plate_num":"粤CS2795","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"2","name":"杨苏委","id_card":"33252219820529438X","phone":"18868646822","total_price":"50"},
{"bus_partner_phone":"8111333","serial_num":"JCZH0930","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/27","depart_time":"9:30:00","get_on_time":"9:30:00","plate_num":"粤C24452","get_on_station":"珠海珠海机场","take_off_station":"珠海南屏","ticket_num":"2","name":"刘红军","id_card":"510224197405286000","phone":"13372683213","total_price":"38"},
{"bus_partner_phone":"8111333","serial_num":"JCZH0900","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/27","depart_time":"9:00:00","get_on_time":"9:00:00","plate_num":"粤C24500","get_on_station":"珠海珠海机场","take_off_station":"珠海南屏","ticket_num":"1","name":"张韦伟","id_card":"370602198111234000","phone":"15169687666","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCTJ0850","short_name":"珠海机场—唐家","ticket_date":"2017/7/27","depart_time":"8:50:00","get_on_time":"8:50:00","plate_num":"粤C16386","get_on_station":"珠海珠海机场","take_off_station":"珠海天鹅酒店","ticket_num":"1","name":"符东梁","id_card":"440402199908239000","phone":"18578237289","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCJZG0910","short_name":"珠海机场—九洲港","ticket_date":"2017/7/27","depart_time":"9:10:00","get_on_time":"9:10:00","plate_num":"粤C19798","get_on_station":"珠海珠海机场","take_off_station":"珠海吉大九州城","ticket_num":"1","name":"杨冰楠","id_card":"150403198807051000","phone":"18689936677","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCJZG0715","short_name":"珠海机场—九洲港","ticket_date":"2017/7/27","depart_time":"7:15:00","get_on_time":"7:15:00","plate_num":"粤C19798","get_on_station":"珠海珠海机场","take_off_station":"珠海九洲港","ticket_num":"1","name":"王悦宜","id_card":"510603199407091000","phone":"18824324077","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCZH2320","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/26","depart_time":"23:20:00","get_on_time":"23:20:00","plate_num":"粤C24500","get_on_station":"珠海珠海机场","take_off_station":"珠海中珠大厦","ticket_num":"1","name":"秦冬瑞","id_card":"342601199712094000","phone":"13865427991","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCZH2320","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/26","depart_time":"23:20:00","get_on_time":"23:20:00","plate_num":"粤C24500","get_on_station":"珠海珠海机场","take_off_station":"珠海中珠大厦","ticket_num":"1","name":"黄智恒","id_card":"440103197309183000","phone":"18922383431","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCTJ1620","short_name":"珠海机场—唐家","ticket_date":"2017/7/26","depart_time":"16:20:00","get_on_time":"16:20:00","plate_num":"粤C19798","get_on_station":"珠海珠海机场","take_off_station":"珠海明珠城轨站","ticket_num":"1","name":"艾雷","id_card":"230302198502216000","phone":"18679308567","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCTJ1250","short_name":"珠海机场—唐家","ticket_date":"2017/7/26","depart_time":"12:50:00","get_on_time":"12:50:00","plate_num":"粤C19794","get_on_station":"珠海珠海机场","take_off_station":"珠海天鹅酒店","ticket_num":"1","name":"陶一心","id_card":"239004198807225000","phone":"13016236887","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1145","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/26","depart_time":"11:45:00","get_on_time":"11:45:00","plate_num":"粤C28666","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"陈翊","id_card":"362226197404240000","phone":"18870538288","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCTJ1120","short_name":"珠海机场—唐家","ticket_date":"2017/7/26","depart_time":"11:20:00","get_on_time":"11:20:00","plate_num":"粤C16324","get_on_station":"珠海珠海机场","take_off_station":"珠海明珠城轨站","ticket_num":"1","name":"李慧敏","id_card":"140442719960405000","phone":"17782314492","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCJZG1010","short_name":"珠海机场—九洲港","ticket_date":"2017/7/26","depart_time":"10:10:00","get_on_time":"10:10:00","plate_num":"粤C21542","get_on_station":"珠海珠海机场","take_off_station":"珠海吉大九州城","ticket_num":"1","name":"范斌铖","id_card":"610104199005138000","phone":"18666967657","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCJZG1010","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/26","depart_time":"10:10:00","get_on_time":"10:10:00","plate_num":"粤C21542","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"王茜","id_card":"460035199007080000","phone":"15595868480","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCJZG0940","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/26","depart_time":"9:40:00","get_on_time":"9:40:00","plate_num":"粤C21549","get_on_station":"珠海珠海机场","take_off_station":"珠海南屏","ticket_num":"1","name":"韩佳豪","id_card":"445224199704230000","phone":"15605167270","total_price":"13"},
{"bus_partner_phone":"8111333","serial_num":"JCZH0845","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/26","depart_time":"8:45:00","get_on_time":"8:45:00","plate_num":"粤C28666","get_on_station":"珠海珠海机场","take_off_station":"珠海南屏","ticket_num":"1","name":"辛奇","id_card":"230704199110190000","phone":"18816811773","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCZH0845","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/26","depart_time":"8:45:00","get_on_time":"8:45:00","plate_num":"粤C28666","get_on_station":"珠海珠海机场","take_off_station":"珠海南屏","ticket_num":"1","name":"司婷婷","id_card":"230183198812210000","phone":"18289748234","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCZH2000","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/25","depart_time":"20:00:00","get_on_time":"20:00:00","plate_num":"粤C28670","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"蒋绍伟","id_card":"432922198306014000","phone":"13798098752","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCZH2000","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/25","depart_time":"20:00:00","get_on_time":"20:00:00","plate_num":"粤C28670","get_on_station":"珠海珠海机场","take_off_station":"珠海南屏","ticket_num":"1","name":"王伟","id_card":"310110196811122000","phone":"18105033212","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCZH2000","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/25","depart_time":"20:00:00","get_on_time":"20:00:00","plate_num":"粤C28670","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"杨玉丽","id_card":"352229199008060000","phone":"18068902806","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCZH2000","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/25","depart_time":"20:00:00","get_on_time":"20:00:00","plate_num":"粤C28670","get_on_station":"珠海珠海机场","take_off_station":"珠海中珠大厦","ticket_num":"1","name":"叶朝波","id_card":"332627197112111000","phone":"13706867558","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1945","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/25","depart_time":"19:45:00","get_on_time":"19:45:00","plate_num":"粤C24415","get_on_station":"珠海珠海机场","take_off_station":"珠海拱北城轨站","ticket_num":"1","name":"叶靖","id_card":"440802199307080000","phone":"13680328249","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1930","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/25","depart_time":"19:30:00","get_on_time":"19:30:00","plate_num":"粤C28678","get_on_station":"珠海珠海机场","take_off_station":"珠海南屏","ticket_num":"1","name":"朱伟","id_card":"342129197301120000","phone":"15055563107","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCJZG1810","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/25","depart_time":"18:10:00","get_on_time":"18:10:00","plate_num":"粤C21543","get_on_station":"珠海珠海机场","take_off_station":"珠海南屏","ticket_num":"1","name":"焦雄伟","id_card":"420682198009230000","phone":"13675110711","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCZH1800","short_name":"珠海机场—拱北中珠","ticket_date":"2017/7/25","depart_time":"18:00:00","get_on_time":"18:00:00","plate_num":"粤C28670","get_on_station":"珠海珠海机场","take_off_station":"珠海中珠大厦","ticket_num":"1","name":"陈伟波","id_card":"440402198511119000","phone":"18818655220","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCJZG1610","short_name":"珠海机场—九洲港","ticket_date":"2017/7/25","depart_time":"16:10:00","get_on_time":"16:10:00","plate_num":"粤C18500","get_on_station":"珠海珠海机场","take_off_station":"珠海吉大九州城","ticket_num":"1","name":"蔡小泉","id_card":"440402196408036000","phone":"13570688167","total_price":"25"},
{"bus_partner_phone":"8111333","serial_num":"JCJZG1610","short_name":"珠海机场—九洲港","ticket_date":"2017/7/25","depart_time":"16:10:00","get_on_time":"16:10:00","plate_num":"粤C18500","get_on_station":"珠海珠海机场","take_off_station":"珠海吉大九州城","ticket_num":"1","name":"蔡小泉","id_card":"440402196408036000","phone":"13570688167","total_price":"25"}
]
    },
    ticketsInfo: function () {
      return [
{"id":"1","name":"张连文","phone":"13688600647","id_card":"370724198210056000"},
{"id":"2","name":"黄承熙","phone":"13702640062","id_card":"440402199308249000"},
{"id":"3","name":"庄跃海","phone":"13379999903","id_card":"460200198507100000"},
{"id":"4","name":"康雨桐","phone":"18689988588","id_card":"350123199006250000"},
{"id":"5","name":"劳晨邦","phone":"15112263201","id_card":"450721199711193000"},
{"id":"6","name":"卢泽基","phone":"13751874402","id_card":"440711198410235000"},
{"id":"7","name":"张亚龙","phone":"15888023515","id_card":"341222198902029000"},
{"id":"8","name":"黄佳琪","phone":"18516503277","id_card":"420624199207082000"},
{"id":"9","name":"吴亦琛","phone":"18616806773","id_card":"420204199205134000"},
{"id":"10","name":"吴斌","phone":"13326689236","id_card":"44040219890925927X"},
{"id":"11","name":"冯祎","phone":"13810147383","id_card":"422126198209132000"},
{"id":"12","name":"姜平","phone":"13961757173","id_card":"320204196707081000"},
{"id":"13","name":"冉雨静","phone":"18290492159","id_card":"52222719870430002X"},
{"id":"14","name":"喻彪","phone":"18523870430","id_card":"522227198803194000"},
{"id":"15","name":"杨静","phone":"15223056993","id_card":"500381198510029000"},
{"id":"16","name":"石建平","phone":"13996288525","id_card":"51028119820515013X"},
{"id":"17","name":"刘洋","phone":"18523872365","id_card":"41040319870715565X"},
{"id":"18","name":"钟燕婷","phone":"13876320931","id_card":"460030199110020000"},
{"id":"19","name":"任立业","phone":"13060475789","id_card":"232301197908150000"},
{"id":"20","name":"叶红庆","phone":"13912451135","id_card":"320219196611261000"},
{"id":"21","name":"杜星宇","phone":"18591995066","id_card":"612701199608131000"},
{"id":"22","name":"邹志刚","phone":"13802804115","id_card":"44010519741215063X"},
{"id":"23","name":"黄柏翰","phone":"13232283799","id_card":"340104196212090000"},
{"id":"24","name":"李欢","phone":"15676000343","id_card":"220281198805291000"},
{"id":"25","name":"陈珊","phone":"15871782147","id_card":"422202198609263000"},
{"id":"26","name":"方熙晨","phone":"15871782147","id_card":"420114200907160000"},
{"id":"27","name":"先婷霜","phone":"13824141272","id_card":"510322199611046000"},
{"id":"28","name":"何小华","phone":"18779172179","id_card":"511321197906242000"},
{"id":"29","name":"朱兰兰","phone":"15392598266","id_card":"14108219940706002X"},
{"id":"30","name":"张巍巍","phone":"18645941225","id_card":"232302199012251000"},
{"id":"31","name":"梁雪丽","phone":"18670562566","id_card":"530322199103020000"},
{"id":"32","name":"谷成","phone":"18670562566","id_card":"430481199010295000"},
{"id":"33","name":"何宇","phone":"15971410973","id_card":"421127198807161000"},
{"id":"34","name":"吴迪","phone":"18742433353","id_card":"21011419931029001X"},
{"id":"35","name":"陈信山","phone":"13823605710","id_card":"420803198202014000"},
{"id":"36","name":"德合拉","phone":"13210229933","id_card":"623027198511173000"},
{"id":"37","name":"庞玲","phone":"13168830989","id_card":"230402199807220000"},
{"id":"38","name":"刘强强","phone":"15915902853","id_card":"412728199506216000"},
{"id":"39","name":"张婧","phone":"17678003265","id_card":"150122199411245000"},
{"id":"40","name":"姚虹权","phone":"18772800000","id_card":"420381197901310000"},
{"id":"41","name":"蒋才纲","phone":"13976384529","id_card":"431281197003106000"},
{"id":"42","name":"庄松辉","phone":"13519816198","id_card":"350521198111057000"},
{"id":"43","name":"赵蕊","phone":"13426005836","id_card":"140107198912303000"},
{"id":"44","name":"肖敏","phone":"13669588083","id_card":"441323199308040000"},
{"id":"45","name":"苏曦浩","phone":"18824988320","id_card":"500383200607242000"},
{"id":"46","name":"陈乐秀","phone":"18824988320","id_card":"510229197012143000"},
{"id":"47","name":"朱文生","phone":"18688166859","id_card":"445224199306206000"},
{"id":"48","name":"张炜宜","phone":"13045819196","id_card":"440711199712035000"},
{"id":"49","name":"陈润东","phone":"18928011995","id_card":"445121199501305000"},
{"id":"50","name":"林金鑫","phone":"18259991070","id_card":"350782199203055000"},
{"id":"51","name":"皮雳","phone":"13317710747","id_card":"511024197407110000"},
{"id":"52","name":"李猛","phone":"15801569651","id_card":"110108198610036000"},
{"id":"53","name":"黄观宝","phone":"17820125901","id_card":"440982198810175000"},
{"id":"54","name":"潘宁馨","phone":"15156513323","id_card":"340103199411301000"},
{"id":"55","name":"孙鸣阳","phone":"15624481331","id_card":"370302199412100000"},
{"id":"56","name":"孟成虎","phone":"13114531506","id_card":"421224199410016000"},
{"id":"57","name":"吴瑞湘","phone":"13727031200","id_card":"430103197605120000"},
{"id":"58","name":"谢思来","phone":"15812605520","id_card":"432501200205080000"},
{"id":"59","name":"陈钰莹","phone":"18929712070","id_card":"440902200001260000"},
{"id":"60","name":"柯天立","phone":"13192266167","id_card":"441622199608310000"},
{"id":"61","name":"曾怀萱","phone":"13192265090","id_card":"441422199703125000"},
{"id":"62","name":"郑兹w理","phone":"18618278576","id_card":"412725198207201000"},
{"id":"63","name":"洪波","phone":"18165606605","id_card":"360121198803287000"},
{"id":"64","name":"钱迪","phone":"18268518255","id_card":"330225199604190000"},
{"id":"65","name":"谭俊","phone":"18621899028","id_card":"511227198308235000"},
{"id":"66","name":"许奇钦","phone":"18819834093","id_card":"445381199807286000"},
{"id":"67","name":"张金洁","phone":"15270970353","id_card":"445381199604016000"},
{"id":"68","name":"吴东州","phone":"15765541210","id_card":"411024198901201000"},
{"id":"69","name":"马亥波","phone":"15157196198","id_card":"330103199903092000"},
{"id":"70","name":"马赫优","phone":"15157196198","id_card":"330602200503141000"},
{"id":"71","name":"李军","phone":"13870841542","id_card":"360124198911031000"},
{"id":"72","name":"张小人","phone":"17731198011","id_card":"130123196508300000"},
{"id":"73","name":"李辉","phone":"13012168011","id_card":"130123197209220000"},
{"id":"74","name":"许杰平","phone":"13456821663","id_card":"341225199109126000"},
{"id":"75","name":"何杰","phone":"13560626836","id_card":"512531197405123000"},
{"id":"76","name":"何佳琪","phone":"13560626836","id_card":"511525200605123000"},
{"id":"77","name":"邹国平","phone":"13916650925","id_card":"362501197303102000"},
{"id":"78","name":"王波","phone":"13697297470","id_card":"420106196609053000"},
{"id":"79","name":"王兴安","phone":"13697297470","id_card":"420502199309011000"},
{"id":"80","name":"安艳艳","phone":"13697297470","id_card":"420500196712111000"},
{"id":"81","name":"洪晓城","phone":"18814185764","id_card":"441581199610238000"},
{"id":"82","name":"党亚红","phone":"13032908912","id_card":"610203197305293000"},
{"id":"83","name":"陶闻韬","phone":"15158030128","id_card":"330102199010172000"},
{"id":"84","name":"陈展","phone":"15919171134","id_card":"350111199512112000"},
{"id":"85","name":"陈思奇","phone":"18934606149","id_card":"420683199508150000"},
{"id":"86","name":"陈诗烨","phone":"18358598264","id_card":"330682199512083000"},
{"id":"87","name":"许漠垠","phone":"15958170218","id_card":"650104198901130000"},
{"id":"88","name":"胡哲","phone":"13216521218","id_card":"42112619951104729X"},
{"id":"89","name":"陈铭心","phone":"13143338342","id_card":"440181199210024000"},
{"id":"90","name":"方静凡","phone":"13760592186","id_card":"445224199512106000"},
{"id":"91","name":"陈囿辰","phone":"13760876651","id_card":"44088219930724882X"},
{"id":"92","name":"张婷","phone":"15387169699","id_card":"420103198205044000"},
{"id":"93","name":"杨苏委","phone":"18868646822","id_card":"33252219820529438X"},
{"id":"94","name":"祝周利","phone":"13611786657","id_card":"33252219820529439X"},
{"id":"95","name":"刘霖妍","phone":"13372683213","id_card":"500105200607060000"},
{"id":"96","name":"刘红军","phone":"13372683213","id_card":"510224197405286000"},
{"id":"97","name":"张韦伟","phone":"15169687666","id_card":"370602198111234000"},
{"id":"98","name":"符东梁","phone":"18578237289","id_card":"440402199908239000"},
{"id":"99","name":"杨冰楠","phone":"18689936677","id_card":"150403198807051000"},
{"id":"100","name":"王悦宜","phone":"18824324077","id_card":"510603199407091000"},
{"id":"101","name":"秦冬瑞","phone":"13865427991","id_card":"342601199712094000"},
{"id":"102","name":"黄智恒","phone":"18922383431","id_card":"440103197309183000"},
{"id":"103","name":"艾雷","phone":"18679308567","id_card":"230302198502216000"},
{"id":"104","name":"陶一心","phone":"13016236887","id_card":"239004198807225000"},
{"id":"105","name":"陈翊","phone":"18870538288","id_card":"362226197404240000"},
{"id":"106","name":"李慧敏","phone":"17782314492","id_card":"140442719960405000"},
{"id":"107","name":"范斌铖","phone":"18666967657","id_card":"610104199005138000"},
{"id":"108","name":"王茜","phone":"15595868480","id_card":"460035199007080000"},
{"id":"109","name":"韩佳豪","phone":"15605167270","id_card":"445224199704230000"},
{"id":"110","name":"辛奇","phone":"18816811773","id_card":"230704199110190000"},
{"id":"111","name":"司婷婷","phone":"18289748234","id_card":"230183198812210000"},
{"id":"112","name":"蒋绍伟","phone":"13798098752","id_card":"432922198306014000"},
{"id":"113","name":"王伟","phone":"18105033212","id_card":"310110196811122000"},
{"id":"114","name":"杨玉丽","phone":"18068902806","id_card":"352229199008060000"},
{"id":"115","name":"叶朝波","phone":"13706867558","id_card":"332627197112111000"},
{"id":"116","name":"叶靖","phone":"13680328249","id_card":"440802199307080000"},
{"id":"117","name":"朱伟","phone":"15055563107","id_card":"342129197301120000"},
{"id":"118","name":"焦雄伟","phone":"13675110711","id_card":"420682198009230000"},
{"id":"119","name":"陈伟波","phone":"18818655220","id_card":"440402198511119000"},
{"id":"120","name":"蔡小泉","phone":"13570688167","id_card":"440402196408036000"},
{"id":"121","name":"蔡小泉","phone":"13570688167","id_card":"440402196408036000"},
{"id":"122","name":"伍芙苇","phone":"13798983616","id_card":"440402198712309000"},
{"id":"123","name":"张远见","phone":"15992697091","id_card":"412722198812063000"},
{"id":"124","name":"张远见","phone":"15992697091","id_card":"412722198812063000"},
{"id":"125","name":"陈轶","phone":"18676698268","id_card":"310107198109173000"},
{"id":"126","name":"伍洁燕","phone":"13427220054","id_card":"440781199709063000"},
{"id":"127","name":"梁佩怡","phone":"13316763333","id_card":"440782199805120000"},
{"id":"128","name":"陈咏琪","phone":"13316763333","id_card":"440782199710010000"},
{"id":"129","name":"邓懿芸","phone":"13316763333","id_card":"440782199808060000"},
{"id":"130","name":"何卓宪","phone":"13316763333","id_card":"440782199711240000"},
{"id":"131","name":"申尚弘","phone":"13316763333","id_card":"440782199805070000"},
{"id":"132","name":"刘永启","phone":"18675834546","id_card":"152601198309284000"},
{"id":"133","name":"侯宇","phone":"18757577785","id_card":"220582199504100000"},
{"id":"134","name":"李振雄","phone":"18247126948","id_card":"153216549877412000"},
{"id":"135","name":"杨仁银","phone":"15258463222","id_card":"331021198706173000"},
{"id":"136","name":"王雪梅","phone":"13581862575","id_card":"513101199602234000"},
{"id":"137","name":"王龙飞","phone":"13581862575","id_card":"411621199308012000"},
{"id":"138","name":"韩孝飞","phone":"13536555056","id_card":"410728198809050000"},
{"id":"139","name":"卢洪排","phone":"18103737005","id_card":"410728197603202000"},
{"id":"140","name":"赵慧敏","phone":"18939193239","id_card":"410803198911270000"},
{"id":"141","name":"乔震","phone":"18638643355","id_card":"41088319880413001X"},
{"id":"142","name":"黄展鹏","phone":"15017175849","id_card":"441900199802234000"},
{"id":"143","name":"罗浩铭","phone":"18819182483","id_card":"44011219970730001X"},
{"id":"144","name":"戴武","phone":"15557577088","id_card":"330381198408241000"},
{"id":"145","name":"薛明","phone":"13858780000","id_card":"330325198204262000"},
{"id":"146","name":"焦顶胜","phone":"13336993969","id_card":"330381198410293000"},
{"id":"147","name":"刘洪","phone":"17765712060","id_card":"500226198402111000"},
{"id":"148","name":"李炎龙","phone":"13809800626","id_card":"440402199512149000"},
{"id":"149","name":"李炎龙","phone":"13809800626","id_card":"440402199512149000"},
{"id":"150","name":"石少坚","phone":"13818781631","id_card":"430522198910092000"}
]
    }
  }
})
