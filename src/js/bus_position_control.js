define(['echarts', 'guangdong', 'vue', 'common'], function (echarts, guangdong, Vue, common) {
  $('#myModal').modal({
    backdrop: true,
    show: false
  })
  var V = new Vue({
    el: '#container',
    data: {
      tableHeader: [
        { title: '序号', prop: 'index' },
        { title: '车牌号', prop: 'name' },
        { title: '驾驶员姓名', prop: 'driver' },
        { title: '驾驶员联系电话', prop: 'phone' },
        { title: '所属运输公司', prop: 'buspartner' },
        { title: '乘客总人数', prop: 'count' },
        { title: '当前站点', prop: 'station' },
        { title: '下一任务', prop: 'nextTask' },
        { title: '下一任务开始时间', prop: 'nextTaskTime' },
        { title: '当天任务清单', prop: 'taskList' },
      ],
      items: [],
      busTitle: '',
      busPartner: '',
      busDetailsHeader: [
        { title: '序号', prop: 'index' },
        { title: '乘车人姓名', prop: 'name' },
        { title: '乘车人电话', prop: 'phone' },
        { title: '乘车人身份证', prop: 'id_card' }
      ],
      busDetails: [],
      InitTimer: [], // 保存每个车辆的时间定时器
      busGps: [], // 保存所有车辆gps数据
      tickets: []
    },
    methods: {
      close () { 
        console.log(1)
      }
    }
  })
  V.items = common.busInfo()
  V.tickets = common.ticketsInfo()
  
  var interval = setInterval(function () {
    V.items.forEach(function (el) {
      el.count = parseInt(Math.random() * 40 + 15)
    })
  }, 5000)

  var map = new BMap.Map("bmap")
  var page = {
    IntervalTime: 3000,
    initMap: function () {
      this.map = map
			map.centerAndZoom('珠海', Zoom) // 初始化地图
		  this.addMapControl()
      var convertor = this.convertor = new BMap.Convertor()
			var Zoom = this._Zoom = 12
			map.enableScrollWheelZoom(true)     // 开启鼠标滚轮缩放
			// 创建地址解析器实例
			var myGeo = this._myGeo = new BMap.Geocoder()
			var _this = this
			map.addEventListener("zoomend", function (e) {
			  _this._Zoom = map.getZoom()
      })
      this.getStationPoint()
      this.index = 0
      this.getBusGps(this.index)
    },
    addMapControl: function () {
      var top_left_control = new BMap.ScaleControl({anchor: BMAP_ANCHOR_TOP_LEFT});// 左上角，添加比例尺
      var top_left_navigation = new BMap.NavigationControl();  //左上角，添加默认缩放平移控件
      var mapType1 = new BMap.MapTypeControl({mapTypes: [BMAP_NORMAL_MAP,BMAP_HYBRID_MAP], anchor: BMAP_ANCHOR_BOTTOM_RIGHT});
      map.addControl(top_left_control);        
		  map.addControl(top_left_navigation); 
      map.addControl(mapType1);          //2D图，卫星图

      var ctrl = new BMapLib.TrafficControl({
        showPanel: false //是否显示路况提示面板
      });      
      map.addControl(ctrl)
      ctrl.setAnchor(BMAP_ANCHOR_TOP_RIGHT)
    },
    // 获取站点坐标
    getStationPoint: function () {
      var stationPoints = common.stationPoint()
      stationPoints.forEach(function(el) {
        var coordinateArr = el.coordinate.match(/[^\D\(\)]+.\d+/g);
        var _point = new BMap.Point(coordinateArr[0], coordinateArr[1])
        this.addStationMarker(el.name, _point)
      }, this)
    },
    // 循环获取车辆gps数据初始化
    getBusGps: function (index) {
      this.busInfo = common.busInfo()
      var path = `../gps/${this.busInfo[index].label}.json`
      common.fetch(path).then(function (res) {
        _this.initBusPosition(index, res.gps)
      })
      var _this = this
      setTimeout(function () {
        _this.index++
        if (_this.index >= _this.busInfo.length) {
          return
        }
        _this.getBusGps(_this.index)
      }, 400);
    },
    // 初始化车辆位置
    initBusPosition: function (index, gps) {
      V.InitTimer[index] = {}
      V.busGps[index] = gps
      var bus_c234 = V.busGps[index][0]
      var _point = new BMap.Point(bus_c234.lng, bus_c234.lat)
      // map.centerAndZoom(_point, 13)
      V.busGps[index].label = this.addBusMarker(this.busInfo[index].name, _point)
      V.InitTimer[index].i = 1
      var _this = this
      V.InitTimer[index].timer = setInterval(function () {
        var bus_c234 = V.busGps[index][V.InitTimer[index].i]
        var _point = new BMap.Point(bus_c234.lng, bus_c234.lat)
        //将得到的经纬度转化为百度地图的
        var pointArr = []
        pointArr.push(_point)
        
        _this.convertor.translate(pointArr, 1, 5, function (data) {
          if (data.status === 0) {
            _point = data.points[0]
            _this.map.removeOverlay(V.busGps[index].label)
            V.busGps[index].label = _this.addBusMarker(_this.busInfo[index].name, _point)
          }
        })
        V.InitTimer[index].i++
        if (V.InitTimer[index].i === V.busGps[index].length) {
          clearInterval(V.InitTimer[index].timer)
          console.log('已走完')
        }
      }, this.IntervalTime)

    },
    // 创建站点标签
    setLabel:function(content,point) {
			var opts = {
			  position : point,    // 指定文本标注所在的地理位置
			  offset   : new BMap.Size(-30, -40)    //设置文本偏移量
			}
			var label = new BMap.Label(content, opts);  // 创建文本标注对象
				label.setStyle({
					 color : "#fff",
					 fontSize : "12px",
					 height : "30px",
					 lineHeight : "30px",
					 fontFamily:"微软雅黑",
					 padding:"0 10px",
           border: "1px solid #ff0000",
           background: '#ff0000'
				 });
			this.map.addOverlay(label); 
    },
    // 创建车辆标签
    addBusLabel: function (content,point) {
      var opts = {
			  position : point,    // 指定文本标注所在的地理位置
			  offset   : new BMap.Size(-10, -30)    //设置文本偏移量
      }
			var label = new BMap.Label(content, opts);  // 创建文本标注对象
				label.setStyle({
					 color : "#fff",
					 fontSize : "14px",
					 height : "20px",
					 lineHeight : "20px",
					 fontFamily:"微软雅黑",
					 padding:"0 15px",
           border: "1px solid #003366",
           background: '#003366'
				 });
        this.map.addOverlay(label)
        return label
    },
    addBusMarker:function(content, point) {	// 创建车辆图标对象
			var myIcon = new BMap.Icon("../img/bus.png", new BMap.Size(45, 63), {anchor: new BMap.Size(10, 25), imageSize:new BMap.Size(23,32)});  
	        // 创建标注对象并添加到地图
	    var busMarker = new BMap.Marker(point, {icon: myIcon})
	    this.map.addOverlay(busMarker)
      busMarker.setTop(true)
      busMarker.setTitle(content)
      busMarker.addEventListener('click', function () {
        V.busDetails = []
        $('#myModal').modal("show")
        V.items.forEach(function (el) {
          if (el.name === content) {
            V.busTitle = content
            V.busPartner = el.buspartner
            for (i = 0; i < el.count; i++) {
              var index = parseInt(Math.random() * 99)
              V.busDetails.push(V.tickets[index])
            }
          }
        })
      })
      return busMarker
		},
    // 创建标记
    addMarker: function (point) {
      var marker = new BMap.Marker(point)
      map.addOverlay(marker)
    },
    // 创建站点标记
    addStationMarker: function (content,point) {
      this.addMarker(point)
      this.setLabel(content, point)
    },
    // 创建停车位覆盖物
    addCircle: function (content, point) {
      var opts = {
			  position : point,    // 指定文本标注所在的地理位置
			  offset   : new BMap.Size(-10, -30)    //设置文本偏移量
      }
			var label = new BMap.Label(content, opts);  // 创建文本标注对象
      label.setStyle({
          color : "#fff",
          fontSize : "14px",
          height : "30px",
          lineHeight : "30px",
          fontFamily:"微软雅黑",
          padding:"0 15px",
          border: "1px solid #21906f",
          background: '#21906f'
        });
      this.map.addOverlay(label)
      return label
    }
  }
  page.initMap()

})