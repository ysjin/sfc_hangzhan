define(['echarts', 'zhuhai', 'vue', 'common'], function (echarts, zhuhai, Vue, common) {
  var V = new Vue({
    el: '#container',
    data: {
         tableHeader:[
            {title:'序号',prop:'index'},
            {title:'乘车站点',prop:'get_on_station'},
            {title:'车牌号',prop:'car_id'},
            {title:'驾驶员姓名',prop:'driver'},
            {title:'驾驶员联系电话',prop:'phone'},
            {title:'所属运输公司',prop:'buspartner'},
            {title:'乘客姓名',prop:'name'},
            {title:'身份证号',prop:'id_card'},
            {title:'车票类型',prop:'type'},
            {title:'乘车日期',prop:'ticket_date'},
            {title:'预定乘车班次',prop:'serial_num'},
            {title:'上车站点',prop:'get_on_station'},
            {title:'验票时间',prop:'get_on_time'},
            {title:'下车站点',prop:'take_off_station'}

         ],
      items: []
    },
    methods: {
    }
  })

  var orderInfoList = common.orderInfo()
  var busInfoList=common.busInfo()
  var p=-1;
    orderInfoList.forEach(function (el) { 
    el.type = '成人票'  
    el.ticket_date = common.getDays().current
    if(p<16){p=p+1}else {p=0}
    el.car_id=busInfoList[p].name;
    el.driver=busInfoList[p].driver;
    el.buspartner=busInfoList[p].buspartner; 
  })

   var lists = orderInfoList.splice(0, 5)
   var Pages = {
    search: function (lists) {
      V.items = lists
    }
  }
  Pages.search(lists)

  var i = 5
  var interval = null
  interFun()
  function interFun() {
    var timer = parseInt(Math.random() * 6 + 3) * 1000
    interval = setInterval(function () {
      var obj = orderInfoList[i]
      obj.createtime = common.getDays().datetime
      obj.ticket_date = common.getDays().current
      V.items.unshift(obj)
      if (V.items.length > 15) {
        V.items.splice(15, 1)
      }
      i++
      if (i >= 99) { 
        i = 0
      }
      clearInterval(interval)
      interFun()
    }, timer)
  }

  var myChart = echarts.init(document.getElementById('main'));
  renderInit()
  function renderInit() {
    var geoCoordMap = {}
    var data = []
    var stationPoints = common.stationPoint()
    stationPoints.forEach(function (el) {
      var coordinateArr = el.coordinate.match(/[^\D\(\)]+.\d+/g)
      geoCoordMap[el.name] = [coordinateArr[0], coordinateArr[1]]
      var obj = {
        name: el.name,
        value: parseInt(Math.random() * 15 + 5)
      }
      data.push(obj)
    }, this);
    console.log(geoCoordMap)
   

    var convertData = function (data) {
      var res = [];
      for (var i = 0; i < data.length; i++) {
        var geoCoord = geoCoordMap[data[i].name];
        if (geoCoord) {
          res.push({
            name: data[i].name,
            value: geoCoord.concat(data[i].value)
          });
        }
      }
      return res;
    };
    
    var convertedData = []
    function updateData() {
      convertedData = [
        convertData(data),
        convertData(data.sort(function (a, b) {
          return b.value - a.value;
        }).slice(0, 6))
      ]
    }  
    updateData()

    option = {
      backgroundColor: '#404a59',
      animation: true,
      animationDuration: 1000,
      animationEasing: 'cubicInOut',
      animationDurationUpdate: 1000,
      animationEasingUpdate: 'cubicInOut',
      title: [
        {
          text: '乘车监控',
          subtext: '',
          sublink: 'http://www.pm25.in',
          left: 'center',
          textStyle: {
            color: '#fff'
          }
        },
        {
          id: 'statistic',
          right: 120,
          top: 40,
          width: 100,
          textStyle: {
            color: '#fff',
            fontSize: 16
          }
        }
      ],
      toolbox: {
        iconStyle: {
          normal: {
            borderColor: '#fff'
          },
          emphasis: {
            borderColor: '#b1e4ff'
          }
        }
      },
      brush: {
        outOfBrush: {
          color: '#abc'
        },
        brushStyle: {
          borderWidth: 2,
          color: 'rgba(0,0,0,0.2)',
          borderColor: 'rgba(0,0,0,0.5)',
        },
        seriesIndex: [0, 1],
        throttleType: 'debounce',
        throttleDelay: 300,
        geoIndex: 0
      },
      geo: {
        map: 'zhuhai',
        left: '10',
        right: '35%',
        center: [113.387199, 22.013837],
        zoom: 0.5,
        label: {
          emphasis: {
            show: false
          }
        },
        roam: true,
        itemStyle: {
          normal: {
            areaColor: '#323c48',
            borderColor: '#111'
          },
          emphasis: {
            areaColor: '#2a333d'
          }
        }
      },
      tooltip: {
        trigger: 'item'
      },
      grid: {
        right: 40,
        top: 100,
        bottom: 40,
        width: '30%'
      },
      xAxis: {
        type: 'value',
        name: '人数',
        scale: true,
        position: 'top',
        boundaryGap: false,
        splitLine: { show: false },
        axisLine: { show: false },
        axisTick: { show: false },
        axisLabel: { margin: 2, textStyle: { color: '#aaa' } },
      },
      yAxis: {
        type: 'category',
        name: '站点',
        nameGap: 16,
        axisLine: { show: false, lineStyle: { color: '#ddd' } },
        axisTick: { show: false, lineStyle: { color: '#ddd' } },
        axisLabel: { interval: 0, textStyle: { color: '#ddd' } },
        data: []
      },
      series: [
        {
          id: 'scatter',
          name: 'pm2.5',
          type: 'scatter',
          coordinateSystem: 'geo',
          data: convertedData[0],
          symbolSize: function (val) {
            return Math.max(val[2] / 10, 8);
          },
          label: {
            normal: {
              formatter: '{b}',
              position: 'right',
              show: false
            },
            emphasis: {
              show: true
            }
          },
          itemStyle: {
            normal: {
              color: '#ddb926'
            }
          }
        },
        {
          // name: 'Top 5',
          id: 'effectScatter',
          type: 'effectScatter',
          coordinateSystem: 'geo',
          data: convertedData[1],
          symbolSize: function (val) {
            return Math.max(val[2] / 10, 8);
          },
          showEffectOn: 'emphasis',
          rippleEffect: {
            brushType: 'stroke'
          },
          hoverAnimation: true,
          label: {
            normal: {
              formatter: '{b}',
              position: 'right',
              show: true
            }
          },
          itemStyle: {
            normal: {
              color: '#f4e925',
              shadowBlur: 10,
              shadowColor: '#333'
            }
          },
          zlevel: 1
        },
        {
          id: 'bar',
          zlevel: 2,
          type: 'bar',
          symbol: 'none',
          itemStyle: {
            normal: {
              color: '#ddb926'
            }
          },
          data: []
        }
      ]
    };


    myChart.on('brushselected', renderBrushed);

    setTimeout(function () {
      myChart.dispatchAction({
        type: 'brush',
        areas: [
          {
            geoIndex: 0,
            brushType: 'polygon',
            coordRange: [[113.186158, 21.869412], [113.017744, 22.467894], [113.295608, 22.646857], [113.295608, 22.646857], [113.594384, 22.589509], [113.681915, 22.254917], [113.572933, 22.091754]]
          }
        ]
      })
    }, 0)


    function renderBrushed(params) {
      var mainSeries = params.batch[0].selected[0];
      var selectedItems = [];
      var categoryData = [];
      var barData = [];
      var maxBar = 30;
      var sum = 0;
      var count = 0;

      for (var i = 0; i < mainSeries.dataIndex.length; i++) {
        var rawIndex = mainSeries.dataIndex[i];
        var dataItem = convertedData[0][rawIndex];
        var pmValue = dataItem.value[2];

        sum += pmValue;
        count++;

        selectedItems.push(dataItem);
      }
      selectedItems.sort(function (a, b) {
        return a.value[2] - b.value[2];
      });

      for (var i = 0; i < Math.min(selectedItems.length, maxBar); i++) {
        categoryData.push(selectedItems[i].name);
        barData.push(selectedItems[i].value[2]);
      }
      console.log(categoryData)
      console.log(barData)
      this.setOption({
        yAxis: {
          data: categoryData
        },
        xAxis: {
          axisLabel: { show: !!count }
        },
        title: {
          id: 'statistic',
          // text: count ? '平均订票数: ' + (sum / count).toFixed(4) : ''
          text: count ? '总人数: ' + sum : ''
        },
        series: {
          id: 'bar',
          data: barData
        }
      });
    }

    console.log(option)
    var timer = setInterval(function () {
      var barData = []
      data.forEach(function (el) {
        el.value = el.value + parseInt(Math.random() * 5 + 1)
        barData.unshift(el.value)
      });
      updateData()
      myChart.setOption({
        series: [
          {
            id: 'bar',
            data: barData
          },
          { 
            id: 'scatter',
            data:  convertedData[0]
          },
          { 
            id: 'effectScatter',
            data:  convertedData[1]
          }
        ]
      });
    }, 3000)
    if (option && typeof option === "object") {
        myChart.setOption(option, true);
    }





  }
})
