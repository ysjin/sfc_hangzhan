define(['echarts', 'guangdong', 'vue', 'common'], function (echarts, guangdong, Vue, common) {
  
  var V = new Vue({
    el: '#container',
    data: {
      tableHeader: [
        { title: '序号', prop: 'index' },
        { title: '车牌号', prop: 'name' },
        { title: '驾驶员姓名', prop: 'driver' },
        { title: '驾驶员联系电话', prop: 'phone' },
        { title: '所属运输公司', prop: 'buspartner' },
        { title: '乘客总人数', prop: 'count' },
        { title: '当前站点', prop: 'station' }
      ],
      items: [],
      routes: [{ id: 1, name: '拱北—珠海机场' }, { id: 2, name: '吉大—珠海机场' }, { id: 3, name: '斗门—珠海机场' }]
    },
    methods: {
    }
  })
  
  V.items = common.busInfo()


  var myCharts = []
  V.routes.forEach(function(el) {
    myCharts.push(echarts.init(document.getElementById('main_'+el.id)))
  }, this)
  var IntervalTime = 5000
  var route1 = {
    renderInit: function () {
      var stations1 = ['中珠大厦(3)', '1a', '2a', '来魅力酒店(6)', '4a', '5a', '城轨珠海站(8)',
      '7a', '8a', '航展馆(10)', '10a', '11a',
      '珠海机场'];
      var routes1 = [V.routes[0].name];
      var data1 = this.data1 = [[0, 0, 3], [0, 1, 6], [0, 2, 9], [0, 3, 6], [0, 4, 7],  [0, 5, 4]];
      var option1 = this.options1 = {
        tooltip: {
          position: 'top'
        },
        title: [],
        singleAxis: [],
        series: []
      };
      var _this = this
      echarts.util.each(routes1, function (route, idx) {
        _this.options1.title.push({
          textBaseline: 'middle',
          top: '50px',//(idx + 0.5) * 100 / 7 + '%',
          text: route
        });
        _this.options1.singleAxis.push({
          left: 200,
          type: 'category',
          boundaryGap: false,
          data: stations1,
          top: '50px',//(idx * 100 / 7 + 50) + '%',
          height: (100 / 7 - 10) + '%',
          axisLabel: {
            interval: 2
          }
        })
        _this.options1.series.push({
          singleAxisIndex: idx,
          coordinateSystem: 'singleAxis',
          type: 'scatter',
          data: [],
          symbolSize: function (dataItem) {
            return dataItem[1] * 4;
          }
        })
      })

      echarts.util.each(data1, function (dataItem) {
        _this.options1.series[dataItem[0]].data.push([dataItem[1], dataItem[2]]);
      });
      myCharts[0].setOption(_this.options1);
      this.realUpdate()
    },
    realUpdate: function () {
      var _this = this
      var options1 = this.options1
      this.i = 0
      this.timer1 = setInterval(function () {
        _this.i++
        _this.data1.forEach(function (el) {
          el[1]++
          if (el[1] !== 12 && el[1] % 3 === 0) {
            el[2] = parseInt(Math.random() * 6 + 3)
            
          } else if (el[1] === 12) {
            el[2] = 0
          }
        })
        options1.singleAxis[0].data = [`中珠大厦(${parseInt(Math.random() * 40 + 5)})`, '1a', '2a', `来魅力酒店(${parseInt(Math.random() * 40 + 5)})`, '4a', '5a', `城轨珠海站(${parseInt(Math.random() * 40 + 5)})`,
        '7a', '8a', `航展馆(${parseInt(Math.random() * 40 + 5)})`, '10a', '11a',
        `珠海机场`]
        options1.series[0].data = []
        echarts.util.each(_this.data1, function (dataItem) {
          options1.series[dataItem[0]].data.push([dataItem[1], dataItem[2]]);
        });
        myCharts[0].setOption(options1);
        if (_this.i > 12) { 
          clearInterval(_this.timer1)
        }
      }, IntervalTime)
    }
  }
  route1.renderInit()


  var route2 = {
    renderInit: function () {
      var stations1 = ['吉大九洲港客运站(20)', '1a', '2a', '拱北锦江之星(23)', '4a', '5a', '南屏华发商都(24)',
      '7a', '8a', '航展馆(13)', '10a', '11a',
      '珠海机场'];
      var routes1 = [V.routes[1].name];
      var data1 = this.data1 = [[0, 0, 3], [0, 1, 6], [0, 2, 9], [0, 3, 6], [0, 4, 7],  [0, 5, 4]];
      var option1 = this.options1 = {
        tooltip: {
          position: 'top'
        },
        title: [],
        singleAxis: [],
        series: []
      };
      var _this = this
      echarts.util.each(routes1, function (route, idx) {
        _this.options1.title.push({
          textBaseline: 'middle',
          top: '50px',//(idx + 0.5) * 100 / 7 + '%',
          text: route
        });
        _this.options1.singleAxis.push({
          left: 200,
          type: 'category',
          boundaryGap: false,
          data: stations1,
          top: '50px',//(idx * 100 / 7 + 50) + '%',
          height: (100 / 7 - 10) + '%',
          axisLabel: {
            interval: 2
          }
        })
        _this.options1.series.push({
          singleAxisIndex: idx,
          coordinateSystem: 'singleAxis',
          type: 'scatter',
          data: [],
          symbolSize: function (dataItem) {
            return dataItem[1] * 4;
          }
        })
      })

      echarts.util.each(data1, function (dataItem) {
        _this.options1.series[dataItem[0]].data.push([dataItem[1], dataItem[2]]);
      });
      myCharts[1].setOption(_this.options1);
      this.realUpdate()
    },
    realUpdate: function () {
      var _this = this
      var options1 = this.options1
      this.i = 0
      this.timer1 = setInterval(function () {
        _this.i++
        _this.data1.forEach(function (el) {
          el[1]++
          if (el[1] !== 12 && el[1] % 3 === 0) {
            el[2] = parseInt(Math.random() * 6 + 3)
          } else if (el[1] === 12) {
            el[2] = 0
          }
        })
        
        options1.series[0].data = []
        options1.singleAxis[0].data = [`吉大九洲港客运站(${parseInt(Math.random() * 40 + 5)})`, '1a', '2a', `拱北锦江之星(${parseInt(Math.random() * 40 + 5)})`, '4a', '5a', `南屏华发商都(${parseInt(Math.random() * 40 + 5)})`,
        '7a', '8a', `航展馆(${parseInt(Math.random() * 40 + 5)})`, '10a', '11a',
        `珠海机场`]
        echarts.util.each(_this.data1, function (dataItem) {
          options1.series[dataItem[0]].data.push([dataItem[1], dataItem[2]]);
        });
        myCharts[1].setOption(options1);
        if (_this.i > 12) { 
          clearInterval(_this.timer1)
        }
      }, IntervalTime)
    }
  }
  
  route2.renderInit()

  var route3 = {
    renderInit: function () {
      var stations1 = ['斗门城市候机楼(10)', '1a', '2a', '3a', '红旗曼哈顿酒店(11)', '5a', '6a',
      '7a', '航展馆(12)', '9a', '10a', '11a', '珠海机场'];
      var routes1 = [V.routes[2].name];
      var data1 = this.data1 = [[0, 0, 3], [0, 1, 6], [0, 2, 9], [0, 3, 3], [0, 4, 6], [0, 5, 9]];
      var option1 = this.options1 = {
        tooltip: {
          position: 'top'
        },
        title: [],
        singleAxis: [],
        series: []
      };
      var _this = this
      echarts.util.each(routes1, function (route, idx) {
        _this.options1.title.push({
          textBaseline: 'middle',
          top: '50px',//(idx + 0.5) * 100 / 7 + '%',
          text: route
        });
        _this.options1.singleAxis.push({
          left: 200,
          type: 'category',
          boundaryGap: false,
          data: stations1,
          top: '50px',//(idx * 100 / 7 + 50) + '%',
          height: (100 / 7 - 10) + '%',
          axisLabel: {
            interval: 3
          }
        })
        _this.options1.series.push({
          singleAxisIndex: idx,
          coordinateSystem: 'singleAxis',
          type: 'scatter',
          data: [],
          symbolSize: function (dataItem) {
            return dataItem[1] * 4;
          }
        })
      })

      echarts.util.each(data1, function (dataItem) {
        _this.options1.series[dataItem[0]].data.push([dataItem[1], dataItem[2]]);
      });
      myCharts[2].setOption(_this.options1);
      this.realUpdate()
    },
    realUpdate: function () {
      var _this = this
      var options1 = this.options1
      this.i = 0
      this.timer1 = setInterval(function () {
        _this.i++
        _this.data1.forEach(function (el) {
          el[1]++
          if (el[1] !== 12 && el[1] % 4 === 0) {
            el[2] = parseInt(Math.random() * 6 + 3)
          } else if (el[1] === 12) {
            el[2] = 0
          }
        })

        options1.series[0].data = []
        options1.singleAxis[0].data = [`斗门城市候机楼(${parseInt(Math.random() * 40 + 5)})`, '1a', '2a', '3a', `红旗曼哈顿酒店(${parseInt(Math.random() * 40 + 5)})`, '5a', '6a',
        '7a', `航展馆(${parseInt(Math.random() * 40 + 5)})`, '9a', '10a', '11a', '珠海机场']
        echarts.util.each(_this.data1, function (dataItem) {
          options1.series[dataItem[0]].data.push([dataItem[1], dataItem[2]]);
        });
        myCharts[2].setOption(options1);
        if (_this.i > 12) { 
          clearInterval(_this.timer1)
        }

        
        V.items.forEach(function (el) {
          el.count = parseInt(Math.random() * 40 + 15)
        })

      }, IntervalTime)

    }
  }
  
  route3.renderInit()
})