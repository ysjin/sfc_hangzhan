
   $('#re_table').hide();   //隐藏表格
   var map = new AMap.Map("container", {      //创建一个map对象
        resizeEnable: true,
        center: [113.562211,22.242966],
        zoom: 13,//地图显示的缩放级别
        keyboardEnable: false
    });

    map.setLang("ch");                    //指定map底图的语言
     
  

     //点击查询后显示所输位置
    function search(){
      var place=document.getElementsByName("keyword");
      var place_value=place[0].value;
     
    AMap.service(["AMap.PlaceSearch"], function() {
        var placeSearch = new AMap.PlaceSearch({       //构造地点查询类
            pageSize: 12,
            city: "0756", //城市
            map: map,
            panel: "panel"
        });
        
        placeSearch.setLang("ch");//设置检索语言类型
        console.log(placeSearch.getLang( ));//获取placeSearch检索语言类型
        placeSearch.search(place_value);//关键字查询后显示所输的位置

        //通过点击结果面板上的列表项来获取坐标，通过坐标求距离
        placeSearch.on('listElementClick',function(e){
             
              var lng=e.marker.getPosition( ).lng;
              var lat=e.marker.getPosition( ).lat;
              var start=new LngLat(lng, lat);
              var add=[   {name: '航展馆',position: [113.385933,22.017763],waiting:26}, 
                          {name:'中珠大厦',position: [113.552358,22.222465],waiting:98}, 
                          {name:'来魅力酒店',position: [113.552093,22.217552],waiting:34},
                          {name:'城轨珠海站',position: [113.550964,22.215868],waiting:70},
                          {name:'九洲港客运站',position: [113.592001,22.239419],waiting:33},
                          {name:'锦江之星（拱北店）',position: [113.555585,22.236928],waiting:17},
                          {name:'华发商都',position: [113.510891,22.224599],waiting:20},
                          {name:'聚龙酒店',position: [113.598183,22.357192],waiting:15},
                          {name:'天鹅大酒店',position: [113.560378,22.278037],waiting:66},
                          {name:'千鹏酒店',position: [113.535249,22.268302],waiting:49},
                          {name:'城轨明珠站',position: [113.515488,22.268738],waiting:51},];

              var distance=[];  //用来存放所输位置与各个站点距离的数组

                for(var p=0;p<add.length;p++){       //循环算出所在位置与每一个站点的距离

                var end= new LngLat(add[p].position[0],add[p].position[1]);
                var name=add[p].name;
                var waiting=add[p].waiting;
                var obj = {}
                obj.dis=(calculateLineDistance(start, end)/1000).toFixed(2);
                obj.name = name;
                obj.waiting=waiting;
                distance.push(obj)
                }

               distance.sort(function (m, n) {      //将距离排序
                   console.log(m)
                   if (Number(m.dis) < Number(n.dis)) return -1
                   else if (Number(m.dis) > Number(n.dis)) return 1
                   else return 0});

               
                
               $('#re_table').show();  //显示隐藏的表格

            
               $('#td1').html(distance[0].name);
               $('#td2').html(distance[0].dis);
               $('#td01').html(distance[0].waiting);

               $('#td3').html(distance[1].name);
               $('#td4').html(distance[1].dis);
               $('#td02').html(distance[1].waiting);

              
               $('#td5').html(distance[2].name);
               $('#td6').html(distance[2].dis);
               $('#td03').html(distance[2].waiting);

               
               $('#td7').html(distance[3].name);
               $('#td8').html(distance[3].dis);
               $('#td04').html(distance[3].waiting);

              
               $('#td9').html(distance[4].name);
               $('#td10').html(distance[4].dis);
               $('#td05').html(distance[4].waiting);

              
               $('#td11').html(distance[5].name);
               $('#td12').html(distance[5].dis);
               $('#td06').html(distance[5].waiting);

              
               $('#td13').html(distance[6].name);
               $('#td14').html(distance[6].dis);
               $('#td07').html(distance[6].waiting);

               
               $('#td15').html(distance[7].name);
               $('#td16').html(distance[7].dis);
               $('#td08').html(distance[7].waiting);

        
             
               $('#td19').html(distance[8].name);
               $('#td20').html(distance[8].dis);
               $('#td09').html(distance[8].waiting);

              
               $('#td21').html(distance[9].name);
               $('#td22').html(distance[9].dis);
               $('#td010').html(distance[9].waiting);

               $('#td23').html(distance[10].name);
               $('#td24').html(distance[10].dis);
               $('#td011').html(distance[10].waiting);
              
             
              
    });
         });

    }


       var markers = [                               //各站点的坐标，用来构建marker点标记
       {position: [113.385933,22.017763]}, 
       {position: [113.552358,22.222465]}, 
       {position: [113.552093,22.217552]},
       {position: [113.550964,22.215868]},
       {position: [113.592001,22.239419]},
       {position: [113.555585,22.236928]},
       {position: [113.510891,22.224599]},
       {position: [113.598183,22.357192]},
       {position: [113.560378,22.278037]},
       {position: [113.535249,22.268302]},
       {position: [113.515488,22.268738]},
       ];

      
       var stationcontent=['航展馆','中珠大厦','来魅力酒店','城轨珠海站', '九洲港客运站','锦江之星（拱北店）','华发商都','聚龙酒店','天鹅大酒店','千鹏酒店','城轨明珠站'];       //用来设置点标记的label的内容
       var i=0;

         markers.forEach(function(marker) {         
        new AMap.Marker({ 
            map: map,
            position: [marker.position[0], marker.position[1]],       //指定marker的坐标
            label:{ content:stationcontent[i++], offset: new AMap.Pixel(17, 17)}          //指定label的内容
        });    
    }); 



//将经纬度封装成对象
            function LngLat(longitude, latitude) {
                this.longitude = longitude;
                this.latitude = latitude;
            }
//计算两个坐标之间的距离
            function calculateLineDistance(start, end) {
                var d1 = 0.01745329251994329;
                var d2 = start.longitude;
                var d3 = start.latitude;
                var d4 = end.longitude;
                var d5 = end.latitude;
                d2 *= d1;
                d3 *= d1;
                d4 *= d1;
                d5 *= d1;
                var d6 = Math.sin(d2);
                var d7 = Math.sin(d3);
                var d8 = Math.cos(d2);
                var d9 = Math.cos(d3);
                var d10 = Math.sin(d4);
                var d11 = Math.sin(d5);
                var d12 = Math.cos(d4);
                var d13 = Math.cos(d5);
                var arrayOfDouble1 = [];
                var arrayOfDouble2 = [];
                arrayOfDouble1.push(d9 * d8);
                arrayOfDouble1.push(d9 * d6);
                arrayOfDouble1.push(d7);
                arrayOfDouble2.push(d13 * d12);
                arrayOfDouble2.push(d13 * d10);
                arrayOfDouble2.push(d11);
                var d14 = Math.sqrt((arrayOfDouble1[0] - arrayOfDouble2[0]) * (arrayOfDouble1[0] - arrayOfDouble2[0]) +
                    (arrayOfDouble1[1] - arrayOfDouble2[1]) * (arrayOfDouble1[1] - arrayOfDouble2[1]) +
                    (arrayOfDouble1[2] - arrayOfDouble2[2]) * (arrayOfDouble1[2] - arrayOfDouble2[2]));

                return(Math.asin(d14 / 2.0) * 12742001.579854401);
            }



             function select_station1(){
                       var select_station=document.getElementById("td1").innerHTML;
                       localStorage.setItem('select_station', select_station);
                       var item=localStorage.getItem('select_station');
                       console.log(item);
                       window.location.href="ticket_purchase.html"

             }

              function select_station2(){
                       var select_station=document.getElementById("td3").innerHTML;
                       localStorage.setItem('select_station', select_station);
                       var item=localStorage.getItem('select_station');
                       console.log(item);
                       window.location.href="ticket_purchase.html"
             }

              function select_station3(){
                       var select_station=document.getElementById("td5").innerHTML;
                       localStorage.setItem('select_station', select_station);
                       var item=localStorage.getItem('select_station');
                       console.log(item);
                       window.location.href="ticket_purchase.html"
             }

              function select_station4(){
                       var select_station=document.getElementById("td7").innerHTML;
                       localStorage.setItem('select_station', select_station);
                       var item=localStorage.getItem('select_station');
                       console.log(item);
                       window.location.href="ticket_purchase.html"
             }

              function select_station5(){
                       var select_station=document.getElementById("td9").innerHTML;
                       localStorage.setItem('select_station', select_station);
                       var item=localStorage.getItem('select_station');
                       console.log(item);
                       window.location.href="ticket_purchase.html"
             }

              function select_station6(){
                       var select_station=document.getElementById("td11").innerHTML;
                       localStorage.setItem('select_station', select_station);
                       var item=localStorage.getItem('select_station');
                       console.log(item);
                       window.location.href="ticket_purchase.html"
             }

              function select_station7(){
                       var select_station=document.getElementById("td13").innerHTML;
                       localStorage.setItem('select_station', select_station);
                       var item=localStorage.getItem('select_station');
                       console.log(item);
                       window.location.href="ticket_purchase.html"
             } 

             function select_station8(){
                       var select_station=document.getElementById("td15").innerHTML;
                       localStorage.setItem('select_station', select_station);
                       var item=localStorage.getItem('select_station');
                       console.log(item);
                       window.location.href="ticket_purchase.html"
             }

              function select_station9(){
                       var select_station=document.getElementById("td19").innerHTML;
                       localStorage.setItem('select_station', select_station);
                       var item=localStorage.getItem('select_station');
                       console.log(item);
                       window.location.href="ticket_purchase.html"
             }
              function select_station10(){
                       var select_station=document.getElementById("td21").innerHTML;
                       localStorage.setItem('select_station', select_station);
                       var item=localStorage.getItem('select_station');
                       console.log(item);
                       window.location.href="ticket_purchase.html"
             }
              function select_station11(){
                       var select_station=document.getElementById("td23").innerHTML;
                       localStorage.setItem('select_station', select_station);
                       var item=localStorage.getItem('select_station');
                       console.log(item);
                       window.location.href="ticket_purchase.html"
             }



         