define(['echarts', 'guangdong', 'vue', 'common'], function (echarts, guangdong, Vue, common) {
  var V = new Vue({
    el: '#container',
    data: {
      tableHeader: [
        { title: '序号', prop: 'index' },
        { title: '停车场名称', prop: 'name' },
        { title: '地址', prop: 'address' },
        { title: '实时空余车位数', prop: 'count' },
        { title: '最后更新时间', prop: 'createtime' }
      ],
      items: [
        { id: 1,  name: '停车场1', count: 10},
        { id: 2,  name: '停车场2', count: 20},      
        { id: 3,  name: '停车场3', count: 30}        
      ],
      parks: [
        { "lng": "113.403833", "lat": "22.18385", "gpstime": "2017/7/25 10:55" },
        {"lng":"113.44765","lat":"22.207416","gpstime":"2017/7/25 10:59"},
        {"lng":"113.372233","lat":"22.013166","gpstime":"2017/7/25 10:59"}
      ]
    },
    methods: {
    }
  })
  
  var map = new BMap.Map("bmap")
  var page = {
    IntervalTime: 1000,
    initMap: function () {
      this.map = map
      var Zoom = this._Zoom = 14
      var _point = new BMap.Point(V.parks[2].lng, V.parks[2].lat)
			map.centerAndZoom(_point, Zoom) // 初始化地图
		  this.addMapControl()
      var convertor = this.convertor = new BMap.Convertor()
			map.enableScrollWheelZoom(true)     // 开启鼠标滚轮缩放
			// 创建地址解析器实例
			var myGeo = this._myGeo = new BMap.Geocoder()
			var _this = this
			map.addEventListener("zoomend", function (e) {
			  _this._Zoom = map.getZoom()
      })
      this.initPark()
    },
    addMapControl: function () {
      var top_left_control = new BMap.ScaleControl({anchor: BMAP_ANCHOR_TOP_LEFT});// 左上角，添加比例尺
      var top_left_navigation = new BMap.NavigationControl();  //左上角，添加默认缩放平移控件
      var mapType1 = new BMap.MapTypeControl({mapTypes: [BMAP_NORMAL_MAP,BMAP_HYBRID_MAP], anchor: BMAP_ANCHOR_TOP_RIGHT});
      map.addControl(top_left_control);        
		  map.addControl(top_left_navigation); 
      map.addControl(mapType1);          //2D图，卫星图
    },
    // 初始停车位
    initPark: function () {
      V.parks.forEach(function(el, index) {
        var _point = new BMap.Point(el.lng, el.lat)
        V.items[index].point = _point
        var count = V.items[index].count
        V.items[index].setLabel = this.addCircle(_point, count)
      }, this)
      
      var _this = this
      // 定义定时器实时更新空位
      var interval = setInterval(function () {
        V.items.forEach(function (el) { 
          el.count = parseInt(Math.random() * 60 + 5)
          map.removeOverlay(el.label)
          el.label = _this.addCircle(el.point, el.count)
        })
      }, 5000)
    },
    // 创建站点标签
    setLabel:function(content,point) {
			var opts = {
			  position : point,    // 指定文本标注所在的地理位置
			  offset   : new BMap.Size(-30, -40)    //设置文本偏移量
			}
			var label = new BMap.Label(content, opts);  // 创建文本标注对象
				label.setStyle({
					 color : "#fff",
					 fontSize : "12px",
					 height : "30px",
					 lineHeight : "30px",
					 fontFamily:"微软雅黑",
					 padding:"0 10px",
           border: "1px solid #ff0000",
           background: '#ff0000'
				 });
			this.map.addOverlay(label); 
    },
    // 创建标签
    addBusLabel: function (content,point) {
      var opts = {
			  position : point,    // 指定文本标注所在的地理位置
			  offset   : new BMap.Size(-40, -40)    //设置文本偏移量
      }
			var label = new BMap.Label(content, opts);  // 创建文本标注对象
				label.setStyle({
					 color : "#fff",
					 fontSize : "14px",
					 height : "30px",
					 lineHeight : "30px",
					 fontFamily:"微软雅黑",
					 padding:"0 15px",
           border: "1px solid #003366",
           background: '#003366'
				 });
        this.map.addOverlay(label)
        return label
    },
    // 创建标记
    addMarker: function (point) {
      var marker = new BMap.Marker(point)
      map.addOverlay(marker)
    },
    // 创建站点标记
    addStationMarker: function (content,point) {
      this.addMarker(point)
      this.setLabel(content, point)
    },
    // 创建停车位覆盖物
    addCircle: function (point, count) {
      var oval = new BMap.Circle(point, 250, {fillColor: 'red', fillOpacity: 1, strokeColor: 'res'})
      map.addOverlay(oval)
      var label = this.addBusLabel(`剩余空位${count}`, point)
      return label
    }
  }
  page.initMap()

})