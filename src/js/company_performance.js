define(['echarts','vue', 'common'], function (echarts,Vue, common) {
var V = new Vue({
   el: '#container',
    data:{
    },
    methods: {
    }

  
  
}
)

var myChart = echarts.init(document.getElementById('performance'),e_macarons);

option = {
    title: {
        text: '运输公司绩效',
       
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    legend: {
        data: ['发车准点率', '车辆故障率']
    },
    grid: {
        left: '3%',
        right: '4%',
        top: '10%',
        containLabel: true
    },
    xAxis: {
        name: '%',
        type: 'value',
       
    },
    yAxis: {
        type: 'category',
        data: ['A运输公司','B运输公司','C运输公司','D运输公司','E运输公司']
    },

   
    series: [
        {
            name: '发车准点率',
            type: 'bar',
            data: [80, 89, 90, 97, 74]
        },
        {
            name: '车辆故障率',
            type: 'bar',
            data: [19, 23, 31, 12, 14]
        }
    ],


};

myChart.setOption(option);






















})