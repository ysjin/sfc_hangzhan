var _config = {
  // JS文件路径前缀
  baseUrl: '../js',
  // JS文件路径声明
  paths: {
    // 第三方开源框架和库
    avalon: ['libs/avalon'],
    vue: ['libs/vue'],
    jquery: ['libs/jquery-2.2.2.min'],
    bootstrap: ['libs/bootstrap-3.3.5-dist/js/bootstrap.min'],
    datetimepicker: ['libs/bootstrap-datetimepicker/bootstrap-datetimepicker'], // 日期选择插件
    datetimepicker_zh_CN:['libs/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN'],
    echarts: ['libs/echarts'],
		china: ['libs/china'],
    guangdong: ['libs/guangdong'],
    zhuhai: ['libs/zhuhai'],

    common: ['utils/common'],
    // 业务
    main: ['../src/main'],
    bookingControl: ['./booking_control'],
    ticket_purchase:['./ticket_purchase'],
    ticket_purchase_en:['./ticket_purchase_en'],
    busPositionControl: ['./bus_position_control'],
    parkControl: ['./park_control'],
    ridingControl: ['./riding_control'],
    routeControl: ['./route_control'],
    station_config:['./station_config'],
    serial_plan:['./serial_plan'],
    company_performance:['./company_performance'],
    host_performance:['./host_performance'],
    help_chinese:['./help_chinese']
  },
  /**
   * JS依赖配置
   */
  shim: {
    // 公共JS依赖配置
    jquery: {
      exports: 'jquery'
    },
    bootstrap: {
      deps: ['jquery']
    },
    datetimepicker: {
      deps: ['jquery', 'bootstrap'],
      exports: 'jquery.datetimepicker'
    },
    datetimepicker_zh_CN: {
      deps: ['datetimepicker']
    },
    echarts: {
      exports: 'echarts'
    }
  }
}
require.config(_config)
