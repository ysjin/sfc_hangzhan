define(['echarts','vue', 'common'], function (echarts,Vue, common) {
var V = new Vue({
   el: '#container',
    data:{
    },
    methods: {
    }

  
  
}
)

var myChart = echarts.init(document.getElementById('performance'),e_macarons);

option = {
    title: {
        text: '航展组织方绩效',
       
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
   
    grid: {
        left: '3%',
        right: '4%',
        top: '10%',
        containLabel: true
    },
    xAxis: {
        name: '分钟',
        type: 'value',
       
    },
    yAxis: {
        type: 'category',
        data: ['车辆平均在途时间', '参观者平均等待时间']
    },
      series : [
        {
            name:'分钟',
            type:'bar',
            barWidth: '40%',
            data:[330, 22]
        }
    ]

   
   

};

myChart.setOption(option);






















})